// Notes:
// The document refers to the whole webpage
// To access specific object models from document we can use:
// document.querySelector('#txt-first-name')

// document.getElementById('txt-first-name')
// document.getElementsByClassName('txt-inputs')
// document.getElementByTagName('input')

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');



// Multiple listeners can also be assigned to the same event
txtFirstName.addEventListener('keyup', (e) => {
	console.log(e.target);
	console.log(e.target.value); // similar to the txtFirstName.value
})

// Mini Activity
// Listen to an event when the last name's input is changed
// either add another event listener or to create a function that will update the span-full-name content

const txtLastName = document.querySelector('#txt-last-name');


const updateFullName = () => {
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('keyup', updateFullName)
txtLastName.addEventListener('keyup', updateFullName)